# Øvingsrepo TDT4109 2023

### Øvings oppgaver

-   [Øving 1](./Ovinger/Oving1/_Oving1.ipynb)
-   [Øving 2](./Ovinger/Oving2/_Oving2.ipynb)
-   [Øving 3](./Ovinger/Oving3/_Oving3.ipynb)
-   [Øving 4](./Ovinger/Oving4/_Oving4.ipynb)
-   [Øving 5](./Ovinger/Oving5/_Oving5.ipynb)
-   [Øving 6](./Ovinger/Oving6/_Oving6.ipynb)
-   Øving 7 (kommer senere)
-   Øving 8 (kommer senere)

### Løsningsforslag

-   [Øving 1](./Losningsforslag/Oving1/)
-   [Øving 2](./Losningsforslag/Oving2/)
-   [Øving 3](./Losningsforslag/Oving3/)
-   Øving 4 (kommer senere)
-   Øving 5 (kommer senere)
-   Øving 6 (kommer senere)
-   Øving 7 (kommer senere)
-   Øving 8 (kommer senere)
